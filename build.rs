use std::env;

fn main() {
    println!("cargo:rustc-env=TARGET={}", env::var("TARGET").unwrap());

    println!(
        "cargo:rustc-env=ARCH={}",
        env::var("CARGO_CFG_TARGET_ARCH").unwrap()
    );

    println!(
        "cargo:rustc-env=ARCH_DIR=../../arch/{}",
        env::var("TARGET").unwrap()
    );

    println!("cargo:rerun-if-changed={}", "arch/x86");
}
