/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

use core::arch::global_asm;

#[cfg(target_arch = "x86")]
global_asm!(include_str!("../../arch/x86/boot/preboot.s"));

#[cfg(target_arch = "x86")]
global_asm!(include_str!("../../arch/x86/boot/multiboot2.s"));
