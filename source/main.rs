/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#![no_std]
#![no_main]

mod boot;

use core::panic::PanicInfo;
use boot::grub2;

#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
	loop {}
}

#[no_mangle]
pub extern "C" fn kmain() -> ! {
	unsafe {
		let vga = 0xb8000 as *mut u64;

		*vga = 0x2f592f412f4b2f4f;
	};

	loop {}
}
