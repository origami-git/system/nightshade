#!/usr/bin/env bash

if (( $# < 2 )); then
	echo "?? usage: run-qemu.sh [target] [iso image]"
	exit 1
fi

if [ "$1" = "x86" ]; then
	if ! command -v qemu-system-i386 &> /dev/null; then
		echo "!! fatal: qemu-system-i386 not installed"
		exit 2
	fi

	qemu-system-i386 \
		-s -S \
		-m 512 \
		-d int \
		-chardev stdio,id=char0,logfile=serial.log,signal=off \
		-serial chardev:char0 \
		-cdrom "$2"
else
	echo "!! fatal: unknown architecture $1"
	exit 3
fi
