#!/usr/bin/env bash

if (( $# < 1 )); then
	echo "!! fatal: no kernel binary file specified"
	exit 1
fi

if command -v grub2-mkrescue &>/dev/null; then
	GRUB_RESCUE_CMD="grub2-mkrescue"
elif command -v grub-mkrescue &>/dev/null; then
	GRUB_RESCUE_CMD="grub-mkrescue"
else
	echo "!! fatal: grub2-mkrescue not found"
	exit 2
fi

PROJECT_DIR="$CARGO_MAKE_CURRENT_TASK_INITIAL_MAKEFILE_DIRECTORY"

kernel="$1"
output="$(dirname $kernel)/kernel.iso"
isoroot="/tmp/xxxxx-grub2-mkiso"

grubcfg="set timeout=3
set default=0

menuentry \"live kernel\" {
	multiboot2 /kernel
	boot
}"

mkdir -p "$isoroot"
rm -r "$isoroot/*"

mkdir -p "$isoroot/boot/grub"

echo "$grubcfg" > "$isoroot/boot/grub/grub.cfg"
cp "$kernel" "$isoroot/kernel"

$GRUB_RESCUE_CMD -o "$output" "$isoroot"
