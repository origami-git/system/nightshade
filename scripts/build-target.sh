#!/usr/bin/env bash

if (( $# < 1 )); then
	echo "!! fatal: no target architecture specified"
	exit 1
fi

PROJECT_DIR="$CARGO_MAKE_CURRENT_TASK_INITIAL_MAKEFILE_DIRECTORY"

ARCH="$1"
TARGET="$PROJECT_DIR/arch/$ARCH.json"

shift

if [ ! -f "$TARGET" ]; then
	echo "!! fatal: architecture not supported: $ARCH"
	exit 2
fi

CWD="$(pwd)"

cd "$PROJECT_DIR"
cargo build $CARGO_FLAGS --target "$TARGET" $@
cd "$CWD"
