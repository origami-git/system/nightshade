.section .multiboot
header_start:
	.long 0xe85250d6              # magic
	.long 0                       # i386 protected mode
	.long header_end - header_start # header length

	# checksum
	.long 0x100000000 - (0xe85250d6 + 0 + (header_end - header_start))

	# required end tag
	.word 0  # type
	.word 0  # flags
	.long 8  # size
header_end:
